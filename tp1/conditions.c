#include <stdio.h>

int main() {

    printf("boucle 1\n");
    for (int i = 0; i <= 1000; ++i) {
        if(i%2 == 0 || i%15 == 0){
            printf("%d \n" , i);
        }
    }

    printf("boucle 2 \n");
    int i = 0;
    while (i < 1001){
        if (i%103 == 0 || i%107 == 0){
            printf("%d \n" , i);
        }
        i++;
    }

    printf("boucle 3 \n");
    i = 0;
    do{
        if ((i%7 == 0 || i%5 == 0) && i%3 != 0){
            printf("%d \n" , i);
        }
        i++;
    }while (i < 1000);

    return 0;
}
