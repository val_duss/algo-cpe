#include <stdio.h>

int main() {
    int i =1;
    float f = 3.3;
    char c = 'c';
    short s = 3;
    long int li = 3333333333;
    long long int lli = 888888888888888888;
    double d = 33.5666666;
    long double ld = 44444444.4444;

    unsigned int ui =1;
    unsigned char uc = 'c';
    unsigned short us = 3;
    unsigned long int uli = 3333333333;
    unsigned long long int ulli = 888888888888888888;

    printf("int %d \n", i);
    printf("float %f \n", f);
    printf("char %c \n", c);
    printf("short %d \n" , s);
    printf("long int %ld \n", li);
    printf("long long int %lld \n", lli);
    printf("double %f \n", d);
    printf("long double %Lf \n", ld);
    printf("unsigned int %u \n", ui);
    printf("unsigned char %c \n", uc);
    printf("unsigned short %u \n" , us);
    printf("unsigned long int %lu \n", uli);
    printf("unsigned long long int %llu \n", ulli);

    return 0;
}
