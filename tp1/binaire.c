#include <stdio.h>

int main() {
    int n = 31;
    int p = 1;
    int temp = n;

    while(p < n){
      p = p * 2;
    }

    printf("Dec : %d to bin: ", n);
    while (temp > 0 && p > 0){
        if(temp - p > 0){
            printf("1");
            temp = temp - p;
        }else{
            printf("0");
        }
        p = p/2;
    }

    printf("\n");
    return 0;
}
