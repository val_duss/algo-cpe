#include <stdio.h>

int main() {
    int compter = 5;
    for (int i = 0; i < compter; ++i) {
        for (int j = 0; j <= i; ++j) {
            if( j!= 0 && j!= i && i != compter -1){
                printf("# ");
            }else{
                printf("* ");
            }
        }
        printf("\n");
    }

    int i = 0;
    int j = 0;
    while (i < compter){
        j = 0;
        while (j <= i){
            if( j!= 0 && j!= i && i != compter -1){
                printf("# ");
            }else{
                printf("* ");
            }
            j++;
        }
        printf("\n");

        i++;
    }
    return 0;
}
