#include <stdio.h>
#include <stdlib.h>

int main(){
    int tab[100];
    int nb = 99933; //searched number

    //create tab
    for(int i=0;i<100; i++){
        tab[i] = rand() % 100000 + 1;
    }

    int cpt = 0;
    //searching
    for(int i=0; i<100; i++){
        if(tab[i] == nb) cpt++;
    }

    printf("L'entier %d est présent %d fois", nb, cpt);

    return 0;
}