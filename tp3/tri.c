#include <stdio.h>
#include <stdlib.h>

int main(){
    int tab[100];

    //create tab
    for(int i=0;i<100; i++){
        tab[i] = rand() % 100000 + 1;
    }

    //display
    for (int i=0; i < 100; ++i)
    {
        printf("%4d \n", tab[i]);
    }

    int tmp = 0;
    //tri bulle
    for (int i=0 ; i < 100; i++)
    {
        for (int j=0 ; j < 100-i; j++)
        {
        if (tab[j] > tab[j+1]) 
        {
            tmp = tab[j];
            tab[j] = tab[j+1];
            tab[j+1] = tmp;
        }
        }
    }

    //display
    for (int i=0; i < 100; ++i)
    {
        printf("%4d \n", tab[i]);
    }

    return 0;
}