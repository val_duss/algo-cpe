#include <stdio.h>
#include <stdlib.h>

struct Couleur{
    int R;
    int G;
    int B;
    int alpha;
};

//Stucture qui enregistre la couleur et le nombre de fois qu'elle est présente dans le tableau
struct Compare{
    struct Couleur c;
    int nb;
};

//Détermine combien de couleurs distinctes (non identiques) il y a
int nbColorsDiff(struct Couleur tab[], int taille)
{
    struct Compare comp[100];

    int i, j;
    int nbv=taille;
    for(i=0; i<taille; i++)
       for(j=i+1; j<taille; j++)
       {
          if(tab[i].R==tab[j].R && tab[i].G==tab[j].G && tab[i].B==tab[j].B && tab[i].alpha==tab[j].alpha)
            {
              nbv--;
              
              break;
            }
       }
    return nbv;
}

int main() {

    struct Couleur tab[100];

    //Déclaration de couleurs aléatoires
    for(int i = 0; i<98; i++){
        tab[i].R = rand()%256;
        tab[i].G = rand()%256;
        tab[i].B = rand()%256;
        tab[i].alpha = rand()%2;
    }

    //Pour tester d'avoir au moins 2 couleurs semblables sinon en random c'est très rare que 2 couleurs soient semblables
    tab[98].R = 21;
    tab[98].G = 07;
    tab[98].B = 14;
    tab[98].alpha = 1;
    tab[99].R = 21;
    tab[99].G = 07;
    tab[99].B = 14;
    tab[99].alpha = 1;


    //Pour tester d'avoir au moins 2 couleurs semblables sinon en random c'est très rare que 2 couleurs soient semblables
    tab[96].R = 21;
    tab[96].G = 07;
    tab[96].B = 14;
    tab[96].alpha = 1;
    tab[97].R = 21;
    tab[97].G = 07;
    tab[97].B = 14;
    tab[97].alpha = 1;

    /*for(int k=0; k< 100; k++){
        printf("Couleur : %d, %d, %d\n", tab[k].R, tab[k].G, tab[k].B);
    }*/

    printf("Nb valeurs distinctes : %d/100\n", nbColorsDiff(tab, 100));

    //I,itialisation du tableau de structures de comparaison
    struct Compare comp[100];
    for(int k=0; k< 100; k++) {
        comp[k].nb = 0;
        comp[k].c.R = tab[k].R;
        comp[k].c.G = tab[k].G;
        comp[k].c.B = tab[k].B;
    }

    //Recherche des doublons (ou triplets, voire plus) il y a
    int i, j;
    int taille = 100; int nbv = 100;
    for(i=0; i<taille; i++){
       for(j=i+1; j<taille; j++)
       {
            if(tab[i].R==tab[j].R && tab[i].G==tab[j].G && tab[i].B==tab[j].B && tab[i].alpha==tab[j].alpha)
            {
                nbv--; int ok = -1;
              for(int k=0; k< 100; k++){
                  if(tab[j].R == comp[k].c.R && tab[j].G == comp[k].c.G && tab[j].B == comp[k].c.B){
                    printf("%d\n", k);
                    if(comp[k].nb == 0) {
                        comp[k].nb=2;
                        comp[k].c.R = tab[i].R;
                        comp[k].c.G = tab[i].G;
                        comp[k].c.B = tab[i].B;   
                    }
                    else comp[k].nb++;
                    break;
                  }
              }
              
              break;
            }
       }
    }

    //Affichage du résultat
    printf("Couleurs étant plusieurs fois (les autres le sont 1 fois) : \n");
    for(int k=0; k< 100; k++){
        if(comp[k].nb != 0) {printf("Couleur : %d, %d, %d, --> %d\n", comp[k].c.R, comp[k].c.G, comp[k].c.B, comp[k].nb);}
    }

    return 0;
}
