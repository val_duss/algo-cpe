#include <stdio.h>
#include <stdlib.h>

int main() {
    //Initialisation des phrases
    char* phrase1 = "Je suis la phrase 1";
    char* phrase2 = "Je s'appelle Groot";
    char* phrase3 = "Je pense qu'il faut qu'on s'adpate";
    char* phrase4 = "C'est tous les jours Halloween";
    char* phrase5 = "Quelle vie si ce n'est celle-ci";
    char* phrase6 = "De quoi tu parles ?";
    char* phrase7 = "Qui aime, like, follow";
    char* phrase8 = "J'aime le tiramisu";
    char* phrase9 = "Je suis puni à cause des autres";
    char* phrase10 = "Les cookies me manquent";

    //Création d'un tab** de phrases
    char** phrases = malloc (10*sizeof(char*));
    phrases[0] = phrase1;
    phrases[1] = phrase2;
    phrases[2] = phrase3;
    phrases[3] = phrase4;
    phrases[4] = phrase5;
    phrases[5] = phrase6;
    phrases[6] = phrase7;
    phrases[7] = phrase8;
    phrases[8] = phrase9;
    phrases[9] = phrase10;

    //Phrases recherchée (à essayer)
    char* phraseCherchee1 = "J'aime le tiramisu";
    char* phraseCherchee2 = "J'aime le sport'";

    //On test
    int oui = 0;
    for(int i=0; i<10; i++){
        if(phrases[i] == phraseCherchee1) oui = 1;
    }

    //On affiche le résultat
    if(oui==1) printf("La phrase fait partie des 10 phrases.\n");
    else printf("La phrase ne fait pas partie des 10 phrases.");

    return 0;
}