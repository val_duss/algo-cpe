#include <stdio.h>
#include <stdlib.h>

int main(){
    int tab[100];

    //create tab
    for(int i=0;i<100; i++){
        tab[i] = rand() % 100000 + 1;
    }

    int min = 101; int max = 0;
    //search max and min
    for(int i=0; i<100; i++){
        if(tab[i] > max) max = tab[i];
        if(tab[i] < min) min = tab[i];
    }

    printf("Min : %d \n", min);
    printf("Max : %d \n", max);

    return 0;
}