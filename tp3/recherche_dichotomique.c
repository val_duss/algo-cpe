#include <stdio.h>
#include <stdlib.h>

int main(){
    int tab[100];

    //create tab
    for(int i=0;i<100; i++){
        tab[i] = rand() % 100000 + 1;
    }

    int tmp = 0;
    //tri bulle
    for (int i=0 ; i < 100; i++)
    {
        for (int j=0 ; j < 100-i; j++)
        {
        if (tab[j] > tab[j+1]) 
        {
            tmp = tab[j];
            tab[j] = tab[j+1];
            tab[j+1] = tmp;
        }
        }
    }

    //display
    for (int i=0; i < 100; ++i)
    {
        printf("%4d \n", tab[i]);
    }

    int i = 0;
    int end = 100;
    int find = 0;
    int middle = 0;
    int nb = 99933; //searched number

    //searching
    while(find == 0 && i < end){
        middle = (i+end)/2;
        if(tab[middle]==nb) find = 1;
        else{
            if(nb > tab[middle]) i = middle + 1;
            else end = middle - 1;
        }
    }

    if(find == 1) printf("L'entier %d est présent au rang %d", nb, middle);
    else printf("L'entier %d n'est pas dans le tableau \n", nb);

    return 0;
}