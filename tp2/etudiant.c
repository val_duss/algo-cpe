#include <stdio.h>

void display(char** tab, int a, int b);

int main(){
    char *student[6][5] = {{"Nom", "Prénom", "Adresse", "Note en C", "Note en OS"},
                {"Duss", "Valentin", "1 rue des lumières", "20", "15"},
                {"Moo", "Aurélien", "37 rue du cinéma", "20", "14"},
                {"Oui", "Louis", "5 rue Moliere", "13", "18"},
                {"Chick", "Clara", "35 rue Armstrong", "17", "16"},
                {"Plouar", "Omar", "68 rue de la boulangerie", "11", "17"},
                }; // array with multi dimension
    
    for(int i = 0; i<6; i++){
        for(int j = 0; j<5; j++){
            printf("%s ", student[i][j]);
        }
        printf("\n");
    }

    return 0;
}