#include <stdio.h>

struct Couleur{
    int R;
    int G;
    int B;
    int alpha;
};

int main() {
    struct Couleur couleur[10];
    for (int i = 0; i < 10; ++i) {
        couleur[i].R = 0xef;
        couleur[i].G = 0xef;
        couleur[i].B = 0xef;
        couleur[i].alpha = 0xef;
    }

    for (int i = 0; i < 10; ++i) {
        printf("couleur[%d]: %d, %d, %d, %d \n", i,couleur[i].R,couleur[i].G, couleur[i].B, couleur[i].alpha);
    }
    return 0;
}
