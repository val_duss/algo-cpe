#include <stdio.h>

int main(){
    int n = 20;
    int u0 = 1;
    int u1 = 1;
    int un = 0;
    int tmp = 0;

    printf("%d, %d", u0, u1);
    for (int i = 2; i<n; i++){ // with for
        un = u1 + u0;

        tmp = u1; //use temp var
        u0 = tmp;
        u1 = un;

        printf(", %d", un);
    }

    printf("\n");

    return 0;
}