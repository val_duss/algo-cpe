#include <stdio.h>

int main() {
    int i =1;
    float f = 3.3;
    char c = 'c';
    short s = 3;
    long int li = 3333333333;
    long long int lli = 888888888888888888;
    double d = 33.5666666;
    long double ld = 44444444.4444;

    unsigned int ui =1;
    unsigned char uc = 'c';
    unsigned short us = 3;
    unsigned long int uli = 3333333333;
    unsigned long long int ulli = 888888888888888888;

    printf("int %d, ptr %llu \n", i , &i);
    printf("float %f, ptr %llu \n", f, &f);
    printf("char %c, ptr %llu \n", c, &c);
    printf("short %d, ptr %llu \n" , s, &s);
    printf("long int %ld, ptr %llu \n", li, &li);
    printf("long long int %lld, ptr %llu \n", lli, &lli);
    printf("double %f, ptr %llu \n", d, &d);
    printf("long double %Lf, ptr %llu \n", ld, &ld);
    printf("unsigned int %u, ptr %llu \n", ui, &ui);
    printf("unsigned char %c, ptr %llu \n", uc, &uc);
    printf("unsigned short %u, ptr %llu \n" , us, &us);
    printf("unsigned long int %lu, ptr %llu \n", uli, &uli);
    printf("unsigned long long int %llu, ptr %llu \n", ulli, &ulli);

    return 0;
}
