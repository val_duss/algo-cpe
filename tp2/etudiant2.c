#include <stdio.h>

struct Etudiant{ //struct
    char* nom;
    char* prenom;
    char* adresse;
    float noteC;
    float noteOS;
};

int main(){
    struct Etudiant etudiant[1];
    etudiant[0].adresse = "avenue des champs élisée";
    etudiant[0].nom = "Valette";
    etudiant[0].prenom = "Frank";
    etudiant[0].noteC = 15.2;
    etudiant[0].noteOS = 10.5;

    printf("Etudiant 0: %s, %s, %s, %f, %f",etudiant[0].prenom, etudiant[0].nom,etudiant[0].adresse,etudiant[0].noteC, etudiant[0].noteOS);

    return 0;
}