#include <stdio.h>
#include <math.h>

int d(long long int entier){
    int temp4 = 0;
    int temp20 = 0;
    for (int i = 0; i < 20; ++i) {//with for
        if(i == 4 && entier%2 == 1){ //check if bit equal 1
            temp4 = 1;
        }

        if(i == 20 && entier%2 == 1){ //check if bit equal 1
            temp20 = 1;
        }
        entier = entier/2;
    }


    printf("%d et %d", (int)(entier/pow(10,3)) , temp4);
    if(temp20 == 1 && temp4 == 1){ //result
        return 1;
    }else{
        return 0;
    }
}


int main() {
    printf("%d \n", d(385));
    printf("%d \n", d(455));

    return 0;
}
