#include <stdio.h>
int char_length(char* chaine){
    int i = 0;
    while(chaine[i] != '\0'){ //whith while the end of char[] is '\0'
        i++;
    }
    return i;
}

char* copy(char * chaine){// use pointer
    char* new_chaine ;
    new_chaine = chaine;

    return new_chaine;
}

char* concat(char * chaine1, char* chaine2){
    char* new_chaine;
    new_chaine = calloc(char_length(chaine1) + char_length(chaine2) + 1, sizeof(char));
    int i;
    for (i = 0; i < char_length(chaine1); ++i) { // with for to browse the char[]
        *(new_chaine + i ) = chaine1[i];
    }
    for (int j = 0; j < char_length(chaine2); ++j) {// with for to browse the char[]
        *(new_chaine + i + j) = chaine2[j]; //add the end id of the last string
    }
    *(new_chaine +  char_length(chaine1) + char_length(chaine2) + 1) = '\0';
    return new_chaine;

}

int main() {
    char c[] = "Bonjour le monde!";
    char c2[] = "Ouiii!";

    copy(c);
    printf("test %d \n", char_length(c));
    printf("test %s \n", copy(c));
    printf("test %s \n", concat(c,c2));
    return 0;
}
