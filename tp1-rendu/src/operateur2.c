#include <stdio.h>

int main() {
    int num1 = 1;
    int num2 = 2;
    char op = '-';


    switch (op) {
        case '+':
            printf("%d", num1 + num2);
            break;
        case '-':
            printf("%d", num1 - num2);
            break;
        case '*':
            printf("%d", num1 * num2);
            break;
        case '/':
            printf("%d", num1 / num2);
            break;
        case '&':
            printf("%d", num1 & num2);
            break;
        case '|':
            printf("%d", num1 | num2);
            break;
        default:
            break;

    }
    return 0;
}
