#include <stdio.h>

int main() {
    int compter = 5;

    //with for
    for (int i = 0; i < compter; ++i) {
        for (int j = 0; j <= i; ++j) {      //two loops to have a "draw" in two dimensions
            if( j!= 0 && j!= i && i != compter -1){     //conditions to remove borders
                printf("# ");
            }else{
                printf("* ");
            }
        }
        printf("\n");
    }

    int i = 0;
    int j = 0;

    //withe while (same result)
    while (i < compter){
        j = 0;
        while (j <= i){     //two loops to have a "draw" in two dimensions
            if( j!= 0 && j!= i && i != compter -1){     //conditions to remove borders
                printf("# ");
            }else{
                printf("* ");
            }
            j++;
        }
        printf("\n");

        i++;
    }
    return 0;
}
