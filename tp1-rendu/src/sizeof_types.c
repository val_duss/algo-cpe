#include <stdio.h>

int main() {
    //size of types 
    printf("Taille de char: %ld\n", sizeof(char));
    printf("Taille de short: %ld\n", sizeof(short));
    printf("Taille de int: %ld\n", sizeof(int));
    printf("Taille de long int: %ld\n", sizeof(long int));
    printf("Taille de long long int: %ld\n", sizeof(long long int));
    printf("Taille de float: %ld\n", sizeof(float));
    printf("Taille de double: %ld\n", sizeof(double));
    printf("Taille de long double: %ld\n", sizeof(long double));

    //size of unsigned types 
    printf("unsigned");
    printf("Taille de unsigned char: %ld\n", sizeof(unsigned char));
    printf("Taille de unsigned short: %ld\n", sizeof(unsigned short));
    printf("Taille de unsigned int: %ld\n", sizeof(unsigned int));
    printf("Taille de unsigned long int: %ld\n", sizeof(unsigned long int));
    printf("Taille de unsigned long long int: %ld\n", sizeof(unsigned long long int));

    return 0;
}
