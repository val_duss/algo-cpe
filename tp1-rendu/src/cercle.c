#define PI 3.1415927
#include <stdio.h>

float Perimetre(float rayon){
    return 2*rayon*PI;
}

float Aire(float rayon) {
    return rayon*rayon*PI;
}

int main() {
    float temp;
    printf("Entrer votre rayon: \n");
    scanf("%f", &temp);
    printf("Perimetre: %f \n" , Perimetre(temp));
    printf("Aire: %f" , Aire(temp));


    return 0;
}
