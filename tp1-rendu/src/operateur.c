#include <stdio.h>

int main() {
    int a = 16;
    int b = 3;

    printf(" plus %d \n", a + b);
    printf(" moins %d \n", a - b);
    printf(" multi %d \n", a * b);
    printf(" div %d \n", (a / b));
    printf(" et %d \n", a && b);
    printf(" ou %d \n", a || b);
    printf(" modulo %d \n", a%b);

    return 0;
}
