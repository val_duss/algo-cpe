#include <stdio.h>
#include <stdlib.h>
#include "fichier.h"

void lire_fichier(char* nom_fichier){
    char ch;
    if(nom_fichier==NULL){
        printf("Erreur lors de l'ouverture du fichier pour lire");
        exit(1);
    }
 
    while((ch=fgetc(nom_fichier))!=EOF){
        printf("%c", ch);
    }


}

void ecrire_dans_fichier(char* nom_fichier, char* message){
        if(nom_fichier==NULL){
            printf("Erreur lors de l'ouverture du fichier pour ecrire");
            exit(1);
        }       
        fputs(message, nom_fichier);
        fclose(nom_fichier);
}

int main(){

    FILE* fichier = NULL; 
    fichier = fopen("text.txt", "a+");
    char message[255];

    lire_fichier(fichier);

    printf("\nLaissez un messsage\n");
    scanf("%[^\n]", &message);
    ecrire_dans_fichier(fichier, message);

    return 0;
}