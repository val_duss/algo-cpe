#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "calcul.h"

int somme(int a, int b){
    return a+b;
}

int difference(int a, int b){
    return a-b;
}

int produit(int a, int b){
    return a*b;
}

int quotient(int a, int b){
    if(b!=0) return a/b;
    else return -1;
}

int modulo(int a, int b){
    return a%b;
}

int et(int a, int b){
    return a && b;
}
int ou(int a, int b){
    return a || b;
}

int negation(int a){
    return -a;
}

int calcul(char* demande) {
    int i = 0;
    char *temp;
    int num1 = 0;
    int num2 = 0;
    temp = malloc(256);
    while(demande[i] != '\0'){
        if(demande[i] >= '0' && demande[i] <= '9'){ //check si c'est un nombre
            temp[strlen(temp)] = demande[i];
        }

        if(demande[i] == ' ' && strcmp(temp, "")){ // si espace alors on entre le nb

            num1 = atoi(temp);
            strcpy(temp,"                                ");//TODO trouver meilleure méthode
        }
        i++;

    }


    num2 = atoi(temp);
    switch (demande[0]) {
        case '+':
            return somme(num1, num2);
        case '-':
            return difference(num1, num2);
        case '*':
            return produit(num1, num2);
        case '/':
            return quotient(num1, num2);
        case '&':
            return et(num1, num2);
        case '|':
            return ou(num1, num2);
        case '~':
            return negation(num1);
        case '%':
            return modulo(num1, num2);
        default:
            break;

    }
    return -113;
}









































