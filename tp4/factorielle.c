#include <stdio.h>
#include <stdlib.h>

int factorielle(int valeur)
{
   if (valeur == 0)
      return 1;
   else
      return valeur * factorielle(valeur - 1);
}

int main()
{
    int n;
    printf("Entrer un entier : ");
    scanf("%d", &n);
    printf("%d! = %d\n",n,factorielle(n));
    return 0;
}