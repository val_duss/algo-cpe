#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void lire_fichier(char* nom_fichier){
    char ch;
    if(nom_fichier==NULL){
        printf("Erreur lors de l'ouverture du fichier pour lire");
        exit(1);
    }
 
    while((ch=fgetc(nom_fichier))!=EOF){
        printf("%c", ch);
    }


}

void ecrire_dans_fichier(char* nom_fichier, char* message, char* message2, char* message3, char* message4, char* message5){
        if(nom_fichier==NULL){
            printf("Erreur lors de l'ouverture du fichier pour ecrire");
            exit(1);
        }       

        fputs(message, nom_fichier);
        fputs(", ", nom_fichier);
        fputs(message2, nom_fichier);
        fputs(", ", nom_fichier);
        fputs(message3, nom_fichier);
        fputs(", ", nom_fichier);
        fputs(message4, nom_fichier);
        fputs(", ", nom_fichier);
        fputs(message5, nom_fichier);
        fputs("\n", nom_fichier);
}


struct Etudiant{ //struct
    char nom[100];
    char prenom[100];
    char adresse[255];
    char noteC[2];
    char noteOS[2];
};

int main(){
    struct Etudiant etudiant;

    FILE* fichier = NULL; 
    fichier = fopen("etu.txt", "a+");

    //lire_fichier(fichier);

    printf("\nPrénom\n");
    scanf("%s", &etudiant.prenom);
    printf("\nNom\n");
    scanf("%s", &etudiant.nom);
    printf("\nVille\n");
    scanf("%s", &etudiant.adresse);
    printf("\nNote C\n");
    scanf("%s", &etudiant.noteC);
    printf("\nNote OS\n");
    scanf("%s", &etudiant.noteOS);

    ecrire_dans_fichier(fichier, etudiant.prenom, etudiant.nom, etudiant.adresse, etudiant.noteC, etudiant.noteOS);

    return 0;
}