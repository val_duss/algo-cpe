#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char** argv) {
    char* toSearch = argv[1];
    char* file = argv[2];
    char *p, *eos;

    FILE* fichier = NULL; 
    fichier = fopen(file, "a+");

    char word[255];
    int cpt=0;

    while (fgets(word,sizeof(word),fichier) != NULL)
    {
        p = word;
        while(eos = strchr( p, ' ' ))
        {
            *eos=0;
            printf( "%s\n", p );
            if(strcmp(toSearch, p)==0) cpt++;
            p += strlen(p) +1;
        }
        puts( p );          
    }

    printf("Le mot '%s' a été trouvé %d fois", toSearch, cpt);

    return 0;
}